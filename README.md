# [winit](https://crates.io/crates/winit)

[**winit**](https://crates.io/crates/winit)
![Crates.io (recent)](https://img.shields.io/crates/dr/winit)
Cross-platform window creation and management
